package mx.cetys.springbootstarter.topic;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Data public class Topic
{
    @Id
    private String id;
    private String name;
    private String description;
}
