package mx.cetys.springbootstarter.course;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CourseRepository extends CrudRepository<Course, String>
{
    public List<Course> getCoursesByName(String name);
    //public List<Course> getCoursesByTopic(String topicId);
}
